﻿using DropDownListForExample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DropDownListForExample.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            CountryViewModel countryModel = new CountryViewModel();
            countryModel.Countries = GetCountries();
            return View(countryModel);
        }

        [HttpPost]
        public ActionResult Index(CountryViewModel countryModel)
        {
            //Now we will get the selected name for display 

            countryModel.Country = (from country in GetCountries() where country.Id == countryModel.SelectedValue select country).FirstOrDefault();

            // reinitialize the country list which is NOT posted back
            countryModel.Countries = GetCountries();

            return View(countryModel);
        }

        public List<Country> GetCountries()
        {
            List<Country> countries = new List<Country>();
            countries.Add(new Country { Id = 1, CountryName = "India", Continent = "Asia" });
            countries.Add(new Country { Id = 2, CountryName = "USA", Continent = "North America" });
            countries.Add(new Country { Id = 3, CountryName = "Pakistan", Continent = "Asia" });
            countries.Add(new Country { Id = 4, CountryName = "Nepal", Continent = "Asia" });
            countries.Add(new Country { Id = 5, CountryName = "Norway", Continent = "Europe" });
            countries.Add(new Country { Id = 6, CountryName = "Egypt", Continent = "Africa" });
            //SelectList objselectlist = new SelectList(objcountry, "Id", "CountryName");
            return countries;
        }

    }
}
