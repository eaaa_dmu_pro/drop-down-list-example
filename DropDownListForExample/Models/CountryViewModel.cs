﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DropDownListForExample.Models
{
    public class CountryViewModel
    {
        public int SelectedValue { get; set; }
        public List<Country> Countries { get; set; }
        public Country Country { get; set; }

        public SelectList GetSelectList()
        {
            return new SelectList(Countries, nameof(Country.Id), nameof(Country.CountryName));
        }
    }
}