﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DropDownListForExample.Models
{
    public class Country
    {
        public int Id { get; set; }
        public string CountryName { get; set; }
        public string Continent { get; set; }
    }
}